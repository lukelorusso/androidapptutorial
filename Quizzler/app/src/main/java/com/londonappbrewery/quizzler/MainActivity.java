package com.londonappbrewery.quizzler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static final TrueFalse[] mQuestionBank = new TrueFalse[] {
            new TrueFalse(R.string.question_1, true),
            new TrueFalse(R.string.question_2, true),
            new TrueFalse(R.string.question_3, true),
            new TrueFalse(R.string.question_4, true),
            new TrueFalse(R.string.question_5, true),
            new TrueFalse(R.string.question_6, false),
            new TrueFalse(R.string.question_7, true),
            new TrueFalse(R.string.question_8, false),
            new TrueFalse(R.string.question_9, true),
            new TrueFalse(R.string.question_10, true),
            new TrueFalse(R.string.question_11, false),
            new TrueFalse(R.string.question_12, false),
            new TrueFalse(R.string.question_13,true)
    };

    private static final int PROGRESS_BAR_INCREMENT =
            /* Returns the smallest (closest to negative infinity) {@code double} value that
               is greater than or equal to the argument and is equal to a mathematical integer. */
            (int) Math.ceil(100.0 / mQuestionBank.length);

    private static final String SCORE_KEY = "SCORE_KEY";
    private static final String INDEX_KEY = "INDEX_KEY";

    private Button mTrueButton;
    private Button mFalseButton;
    private TextView mQuestionTextView;
    private TextView mScoreTextView;
    private ProgressBar mProgressBar;
    private int mIndex;
    private int mScore;
    private int mQuestion;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SCORE_KEY, mScore);
        outState.putInt(INDEX_KEY, mIndex);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTrueButton = findViewById(R.id.true_button);
        mFalseButton = findViewById(R.id.false_button);
        mScoreTextView = findViewById(R.id.score);
        mProgressBar = findViewById(R.id.progress_bar);
        mQuestionTextView = findViewById(R.id.question_text_view);

        if (savedInstanceState != null) {
            mScore = savedInstanceState.getInt(SCORE_KEY);
            mIndex = savedInstanceState.getInt(INDEX_KEY);
            updateScore();
            mProgressBar.setProgress(PROGRESS_BAR_INCREMENT * mIndex);
        }

        mQuestion = mQuestionBank[mIndex].getQuestionId();
        mQuestionTextView.setText(mQuestion);

        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
                updateQuestion();
            }
        });

        mFalseButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
                updateQuestion();
            }
        });

    }

    private void updateQuestion() {
        mIndex = (mIndex + 1) % mQuestionBank.length;

        if (mIndex == 0) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Game Over");
            alert.setCancelable(false);
            alert.setMessage("You scored " + mScore + "points!");
            alert.setPositiveButton("Close Application", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.show();
        }

        mQuestion = mQuestionBank[mIndex].getQuestionId();
        mQuestionTextView.setText(mQuestion);
        mProgressBar.incrementProgressBy(PROGRESS_BAR_INCREMENT);
    }

    private void checkAnswer(boolean selection) {
        boolean correctAnswer = mQuestionBank[mIndex].isAnswer();
        boolean isSelectionCorrect = selection == correctAnswer;

        Toast.makeText(
                this,
                isSelectionCorrect ? R.string.correct_toast : R.string.incorrect_toast,
                Toast.LENGTH_SHORT
        ).show();

        if (isSelectionCorrect) {
            mScore++;
            updateScore();
        }
    }

    private void updateScore() {
        String text = "Score " + mScore + "/" + mQuestionBank.length;
        mScoreTextView.setText(text);
    }
}
