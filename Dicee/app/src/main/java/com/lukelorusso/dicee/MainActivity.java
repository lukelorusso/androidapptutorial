package com.lukelorusso.dicee;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private View mRollBtn;
    private ImageView mLeftDice, mRightDice;

    private int[] diceArray = {
            R.drawable.dice1,
            R.drawable.dice2,
            R.drawable.dice3,
            R.drawable.dice4,
            R.drawable.dice5,
            R.drawable.dice6,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRollBtn = findViewById(R.id.roll_btn);
        mLeftDice = findViewById(R.id.dice_left);
        mRightDice = findViewById(R.id.dice_right);
        this.randomDice();

        mRollBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                randomDice();
            }
        });
    }

    private void randomDice() {
        Random r = new Random();
        mLeftDice.setImageResource(
                diceArray[r.nextInt(6)]
        );
        mRightDice.setImageResource(
                diceArray[r.nextInt(6)]
        );
    }
}
