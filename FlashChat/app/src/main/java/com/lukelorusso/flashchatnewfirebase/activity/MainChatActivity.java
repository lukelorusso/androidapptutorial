package com.lukelorusso.flashchatnewfirebase.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.lukelorusso.flashchatnewfirebase.adapter.ChatListAdapter;
import com.lukelorusso.flashchatnewfirebase.model.InstantMessage;


public class MainChatActivity extends AppCompatActivity {

    private String mDisplayName;
    private ListView mChatListView;
    private EditText mInputText;
    private ImageButton mSendButton;
    private DatabaseReference mDatabaseReference;
    private ChatListAdapter mListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.lukelorusso.flashchatnewfirebase.R.layout.activity_main_chat);

        setupDisplayName();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();

        // Link the Views in the layout to the Java code
        mInputText = findViewById(com.lukelorusso.flashchatnewfirebase.R.id.messageInput);
        mSendButton = findViewById(com.lukelorusso.flashchatnewfirebase.R.id.sendButton);
        mChatListView = findViewById(com.lukelorusso.flashchatnewfirebase.R.id.chat_list_view);

        mInputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                sendMessage();
                return true;
            }
        });
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
    }

    private void setupDisplayName() {
        SharedPreferences prefs = getSharedPreferences(RegisterActivity.CHAT_PREFS, MODE_PRIVATE);
        mDisplayName = prefs.getString(RegisterActivity.DISPLAY_NAME_KEY, "Anonymous");
    }

    // Grab the text the user typed in and push the message to Firebase
    private void sendMessage() {
        String input = mInputText.getText().toString();
        if (input.length() < 1) return;

        InstantMessage chatMsg = new InstantMessage(input, mDisplayName);
        mDatabaseReference.child("messages").push().setValue(chatMsg);
        mInputText.setText("");
    }

    // Setup the adapter here.
    @Override
    protected void onStart() {
        super.onStart();
        mListAdapter = new ChatListAdapter(this, mDatabaseReference, mDisplayName);
        mChatListView.setAdapter(mListAdapter);
    }

    // Remove the Firebase event listener on the adapter.
    @Override
    public void onStop() {
        super.onStop();
        mListAdapter.cleanUp();
    }

}
